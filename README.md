# annuaire-prada

Ce dépot contient une archive quotidienne de l'annuaire des PRADAs maintenu par la CADA.

Ces fichiers sont produits en scrapant l'annuaire en ligne, qui semble plus à jour que la version CSV
disponible sur www.cada.fr.

Le code qui génère ces fichiers se trouve dans [ce dossier](https://gitlab.com/madada-team/dada-core/-/tree/master/db_sync?ref_type=heads) et est executé toutes les nuits via [ce fichier](https://gitlab.com/madada-team/dada-core/-/blob/master/.gitlab-ci.yml?ref_type=heads).

Il contient 3 fichiers:

- `annuaire_prada_web.csv`: le résultat du scraping de l'annuaire disponible [ici](https://www.cada.fr/administration/personnes-responsables). Toutes les entités listées dans cet annuaire sont incluses ici, la donnée n'est pas retravaillée, sauf pour extraire le code postal, et éventuellement corriger quelques erreurs mineurs (coquilles...).
- `cada_directory_dict.json`: un dict/hash json indexé par les identifiants internes MaDada, et contenant les données correspondantes de la CADA. Ce fichier contient toutes les entitées qui ont pu être liées entre l'annuaire de la CADA et les entités de la base de donnée de Ma Dada. Il est probablement incomplet, et le lien étant de nature probabiliste, il n'est pas forcement exact.
- `diff.csv`: la liste des lignes qui ont changé depuis la dernière execution du scraper. Si le fichier ne contient qu'une ligne d'en-têtes, c'est qu'aucun changement n'a été détecté.